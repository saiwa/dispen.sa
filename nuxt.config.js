export default {
    ssr: false,
    srcDir: "resources/nuxt/",

    head: {
        title: "dispen.sa Manage your bread stocks",
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width-device-width, initial-scale=1"
            },
            {
                hid: "description",
                name: "description",
                content: "Not everyone can store as much bread as I can"
            }
        ]
    },

    css: [],
    plugins: [],

    components: true,

    buildModules: [],

    modules: [],

    build: {
        publicPath: process.env.NODE_ENV === "production" ? "assets/" : null,
        extractCSS: true
    },

    generate: {
        dir: "nuxt-public"
    },

    server: {
        port: 3000, // defaults: 3000,
        host: "192.168.33.77" // default: localhost
    },

    watchers: {
        webpack: {
            aggregateTimeout: 300,
            poll: 500
        }
    }
};
